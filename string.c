/* Custom versions of standard string functions. */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

// Find the length of a string, assuming it has a null terminator (like strlen).
int length(const char s[]) {
    int count = 0;
    for (size_t i = 0; s[i] != '\0'; i++) {
      count = count + 1;
    }
    return count;
}

// Copy string s into the character array t (like strcpy).
void copy(char t[], const char s[]) {
  int j = 0;
  for (size_t i = 0; s[i] != '\0'; i++) {
    t[i] = s[i];
    j = i;
  }
  if (length(s) == 0) t[j] = '\0';
  else t[j + 1] = '\0';
}

// Compare two strings, returning negative, zero or positive (like strcmp).
int compare(const char s[], const char t[]) {
    int compare = 0;
    int i = 0;
    int l1, l2;
    l1 = length(s);
    l2 = length(t);
    while (s[i] != '\0'){
      if (t[i] == '\0') compare = 1;
      if (s[i] > t[i]) compare = -1;
      if (s[i] < t[i]) compare = 1;
      i++;
    }
    if (t[i] != '\0') compare = -1;
    if (l1 > l2) compare = 1;
    return compare;
}

// Join string s to the end of string t (like strcat).
void append(char t[], const char s[]) {
  int i = length(t);
  int j = length(s);
  for (int k = 0; s[k] != '\0'; k++) {
    t[i + k] = s[k];
  }
  t[i + j] = '\0';
}

// Find the (first) position of s in t, or return -1 (like strstr).
int find(const char t[], const char s[]) {
    int find = -1;
    int count = 0;
    int l1 = length(s);
    if (l1 == 1){
      while ((t[count] != '\0') && (find == -1)) {
        if (t[count] == s[0]) find = count;
        count = count + 1;
      }
    }
    if (l1 > 1){
      int z = 1;
      while ((t[count] != '\0') && (find == -1)) {
        if (t[count] == s[0]) {
          int i = 1;
          while (i < l1) {
            if (t[count + i] == s[i]) z = z + 1;
            i = i + 1;
          }
          if (z == l1) find = count;
          else find = -1;
        }
        count = count + 1;
      }
    }
    return find;
}

// -----------------------------------------------------------------------------
// Tests and user interface

// Tests 1 to 5
void testLength() {
    assert(length("") == 0);
    assert(length("c") == 1);
    assert(length("ca") == 2);
    assert(length("cat") == 3);
    char s[] = "dog";
    assert(length(s) == 3);
}

// Tests 6 to 9
void testCopy() {
    char t[10];
    copy(t, "cat");
    assert(t[0] == 'c' && t[1] =='a' && t[2] == 't' && t[3] =='\0');
    copy(t, "at");
    assert(t[0] == 'a' && t[1] =='t' && t[2] =='\0');
    copy(t, "t");
    assert(t[0] == 't' && t[1] =='\0');
    copy(t, "");
    assert(t[0] == '\0');
}

// Tests 10 to 17
void testCompare() {
    assert(compare("cat", "dog") < 0);
    assert(compare("dog", "cat") > 0);
    assert(compare("cat", "cat") == 0);
    assert(compare("an", "ant") < 0);
    assert(compare("ant", "an") > 0);
    assert(compare("", "a") < 0);
    assert(compare("a", "") > 0);
    assert(compare("", "") == 0);
}

// Tests 18 to 20
void testAppend() {
    char t[10] = "cat";
    append(t, "");
    assert(t[0] == 'c' && t[1] =='a' && t[2] == 't' && t[3] =='\0');
    t[2] = '\0';
    append(t, "t");
    assert(t[0] == 'c' && t[1] =='a' && t[2] == 't' && t[3] =='\0');
    t[0] = '\0';
    append(t, "cat");
    assert(t[0] == 'c' && t[1] =='a' && t[2] == 't' && t[3] =='\0');
}

// Tests 21 to 25
void testFind() {
    assert(find("cat", "cat") == 0);
    assert(find("cat", "c") == 0);
    assert(find("cat", "t") == 2);
    assert(find("cat", "x") == -1);
    assert(find("banana", "an") == 1);
}

// Test the functions.
int main() {
    testLength();
    testCopy();
    testCompare();
    testAppend();
    testFind();
    printf("Tests all pass.");
    return 0;
}
